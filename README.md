# Step mvn-project-name

Exports the project name in the `pom.xml` as the `${MVN_PROJECT_NAME}`
environment variable.  The resulting variable ois build using the value
of the `groupId` field (removing the first two elements) and the value
of the `artifactId` field.

Given the following chunk from a `pom.xml` file:

```xml
<groupId>com.example.project</groupId>
<artifactId>component</artifactId>
```

The resulting variable would have the value:

```bash
MVN_PROJECT_NAME=project-component
```

Using environment variable allows to use its value in the next steps
defined in the `wercker.yml` file.

# Options

This steps does not have any option.

# Example

```yaml
build:
  after-steps:
    - bigtruedata/mvn-project-name
```

# License

The MIT License (MIT)

# Changelog

## 0.1.0

- Initial release

