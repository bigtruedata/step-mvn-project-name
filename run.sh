#!/bin/bash

GROUP=$(grep '<groupId>' pom.xml \
        | head --lines=1 \
        | cut --delimiter='>' --fields=2 \
        | cut --delimiter='<' --field=1 \
        | cut --delimiter='.' --fields=3-)

ARTIFACT=$(grep '<artifactId>' pom.xml \
           | head --lines=1 \
           | cut --delimiter='>' --fields=2 \
           | cut --delimiter='<' --field=1)

export MVN_PROJECT_NAME="${GROUP}-${ARTIFACT}"
